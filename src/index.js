import React, {useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import {Router} from "@reach/router";
import Home from "./pages/Home";
import Price from "./pages/Price";
import * as serviceWorker from './serviceWorker';
import './index.css';
import Header from "./components/Header";
import {ModelSelectorProvider} from "./components/ModelSelector/context";


const App = () => {
  const [offline, setOffline] = useState(!navigator.onLine);

  useEffect(() => {
    const setOfflineStatus = () => {
      setOffline(!navigator.onLine);
    };

    window.addEventListener('online',  setOfflineStatus);
    window.addEventListener('offline',  setOfflineStatus);

    return () => {
      window.addEventListener('online',  setOfflineStatus)
      window.addEventListener('offline',  setOfflineStatus)  
    }
  }, []);

  const classes = ["theme-default"];

  if (offline) {
    classes.push("offline");
  }
  return (
    <div className={classes.join(" ")}>
      <Header text="nuro" />
      <ModelSelectorProvider>
        <Router>
          <Home path="/" />
          <Price path="/price/:make/:model/:year/:city" />
        </Router>
      </ModelSelectorProvider>
    </div>
  );
};

ReactDOM.render( <App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
