workbox.skipWaiting()
workbox.clientsClaim()

workbox.routing.registerRoute(
  /^https:\/\/fonts\.gstatic\.com/,
  workbox.strategies.cacheFirst({
    cacheName: 'google-fonts-webfonts',
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
      new workbox.expiration.Plugin({
        maxAgeSeconds: 60 * 60 * 24 * 365,
        maxEntries: 30,
      }),
    ],
  })
);

// cache json response
const dataHandler = workbox.strategies.networkFirst({
  cacheName: 'data-cache'
});

workbox.routing.registerRoute(
  new RegExp("(http|https)://.*.*\.json"),
  ({event}) => {
    return dataHandler.handle({event})
      .then((response) => {
        if (response) {
          return response;
        }

        return new Response(
          JSON.stringify({error: 'Unable to fetch data in offline mode.'}), {
            headers: {'Content-Type': 'application/json'}
          }
        );
      }).catch(err => {
        return new Response(
          JSON.stringify({error: 'Unable to fetch data in offline mode.'}), {
            headers: {'Content-Type': 'application/json'}
          }
        );
      })
  }
)

// workbox.routing.registerRoute(
//   new RegExp("(http|https)://.*.*\.json"),
//   workbox.strategies.networkFirst({
//     cacheName: 'data-cache'
//   })
// )

workbox.routing.registerNavigationRoute('index.html');
workbox.precaching.precacheAndRoute(self.__precacheManifest || [])