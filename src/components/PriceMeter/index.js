import React from "react";
import "./price-meter.css";

const PriceMeter = React.memo(({mini = false, text, description}) => {
  const classes = ["price-meter-outer"];
  if (mini) {
    classes.push("mini");
  }
  return (
    <div className={classes.join(" ")}>
      <div className="price-meter">
        <div className="price-meter-inner">
          <div className="price-meter-value">
            {text}
          </div>
        </div>
      </div>
      <div className="price-meter-description">
        {description}
      </div>
    </div>
  )
});


export default PriceMeter;