import React from "react";

const PriceMeterContext = React.createContext();
const {Provider, Consumer} = PriceMeterContext;

const initialState = {
  estimatedDaysBookedPerMonth: "",
  estimatedEarningsPerBookedDay: {},
  estimatedVehicleValue: {},
  recommendedDailyPrice: {},
  error: null
};

class PriceMeterProvider extends React.Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  componentDidMount() {
    this.fetchPrice(this.props);
  }

  componentDidUpdate(prevProps) {
    const {make: _make, model: _model, year: _year, city: _city} = prevProps
    const {make, model, year, city} = this.props;
    if (make !== _make || model !== _model || year !== _year || city !== _city) {
      this.fetchPrice(this.props);
    }
  }

  fetchPrice = (props) => {
    const {make, model, year} = props;
    fetch(`/mock/prices/${year}/${make}/${model}/data.json`)
      .then(resp => resp.json())
      .then((data = {}) => {
        const {error} = data;
        if (error) {
          this.setState({...initialState, error});
        } else {
          this.setState({...data, error: null});
        }
      });
  }

  render() {
    const {error} = this.state;
    const _children = error ? (<div className="price-meters error-box">{error}</div>) : this.props.children;
    return (
      <Provider value={this.state}>
        {_children}
      </Provider>
    );
  }
};

export {PriceMeterContext, PriceMeterProvider, Consumer as PriceMeterConsumer};