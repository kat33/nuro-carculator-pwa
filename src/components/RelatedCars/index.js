import React, {useContext} from "react";
import LinkButton from "../LinkButton";
import {ModelSelectorContext} from "../ModelSelector/context";
import "./related-cars.css"

const RelatedCars = () => {
  const {selected = {}} = useContext(ModelSelectorContext);
  const {city} = selected;
  return (
    <React.Fragment>
      <div className="title">Explore other cars’ earnings</div>
      <div className="related-cars">  
        <LinkButton to={`/price/honda/civic/2018/${city}`} text="Honda Civic" />
        <LinkButton to={`/price/nissan/pathfinder/2018/${city}`} text="Nissan Pathfinder" />
        <LinkButton to={`/price/honda/crv/2018/${city}`} text="Honda CRV" />
        <LinkButton to={`/price/mazda/cx-9/2018/${city}`} text="Mazda CX-9" />
        <LinkButton to={`/price/toyota/rav4/2018/${city}`} text="Toyota RAV4" />
        <LinkButton to={`/price/mazda/cx-5/2018/${city}`} text="Mazda CX-5" />
      </div>
    </React.Fragment>
  );
}

export default RelatedCars;