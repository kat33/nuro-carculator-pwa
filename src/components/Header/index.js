import React from 'react';
import "./header.css"
import { Link } from "@reach/router"

const Header = React.memo(({text, badgeText = "offline"}) => (
  <div className="header clearfix">
    <div className="badge">
      {badgeText}
    </div>
    <div className="logo">
      <Link to="/">{text}</Link>
    </div>
  </div>
));

export default Header;