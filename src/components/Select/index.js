
import React from "react";
import "./select.css";

const Select = React.memo(({name, selectedValue, label = '', children = [], onChange = () => {}}) => {
  return (
    <div className="select-container">
    <label className="select-label">{label}</label>
    <select name={name} defaultValue={selectedValue} className="select-input" onChange={onChange}>
      {children}
    </select>
    </div>
  );
});

export default Select;