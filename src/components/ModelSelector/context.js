import React from "react";

const ModelSelectorContext = React.createContext();
const {Provider, Consumer} = ModelSelectorContext;


class ModelSelectorProvider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      makes: [],
      models: [],
      selected: {
        year: "2018",
        city: "San francisco, CA",
        make: "mazda",
        model: "3"
      },
      onOptionChange: this.onOptionChange
    };
  }

  componentDidMount() {
    const makes = fetch("/mock/makes/data.json").then(resp => resp.json());
    const models = fetch("/mock/models/mazda/data.json").then(resp => resp.json());

    Promise.all([makes, models])
      .then(([makes, models]) => {
        this.setState({
          makes: makes.makes,
          models: models.models
        });
      });
  }

  onOptionChange = (ev) => {
    const type = ev.target.name;
    const value = ev.target.value;
    switch(type) {
      case "model":
      case "year":
      case "city": {
        this.setState({
          selected: {...this.state.selected, [type]: value}
        })
        break;
      }
      case "make": {
        fetch(`/mock/models/${value}/data.json`)
        .then(resp => resp.json())
        .then((models) => {
          const _models = models.models;
          this.setState({
            models: _models,
            selected: {...this.state.selected, model: _models[0].label, make: value}
          });
        });
        break;
      }
      default:
        break;
    }
  }

  render() {
    return (
      <Provider value={this.state}>
        {this.props.children}
      </Provider>
    );
  }
};

export {ModelSelectorContext, ModelSelectorProvider, Consumer as ModelSelectorConsumer};