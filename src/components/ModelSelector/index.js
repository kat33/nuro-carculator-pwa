import React, {useContext} from "react";
import {navigate} from "@reach/router"
import Select from "../Select";
import "./model-selector.css";
import {ModelSelectorContext} from "./context";

const ModelSelector = () => {
  const {makes, models, selected = {}, onOptionChange = () => {}} = useContext(ModelSelectorContext);
  const {make, model, year, city} = selected;
  return (
    <div className="model-selector">
      <Select name="year" label="Year" selectedValue={year} onChange={onOptionChange}>
        <option value="2018">2018</option>
        <option value="2017">2017</option>
      </Select>
      <Select name="make" selectedValue={make} label="Make" onChange={onOptionChange}>
        {makes.map((make) => (
          <option key={make.value} value={make.value}>{make.label}</option>
        ))}
      </Select>
      <Select name="model" selectedValue={model} label="Model" onChange={onOptionChange}>
        {models.map((model) => (
          <option key={model.id} value={model.label}>{model.label}</option>
        ))}
      </Select>
      <Select name="city" selectedValue={city} label="Near" onChange={onOptionChange}>
        <option value="San francisco, ca">San Francisco, CA</option>
        <option value="Los Angeles, CA">Los Angeles, CA</option>
        <option value="Houston, TX">Houston, TX</option>
        <option value="Austin, TX">Austin, TX</option>
      </Select>
      <button className="btn-primary" onClick={() => {
        navigate(`/price/${make}/${model}/${year}/${city}`)
      }}>
        Explore
      </button>
    </div>
  )
}

export default ModelSelector;