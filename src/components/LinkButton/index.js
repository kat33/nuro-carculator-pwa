import React from "react";
import {Link} from "@reach/router";
import "./link-button.css";

const LinkButton = React.memo(({to, text, className = ""}) => {
  return (
    <Link to={to} className={["link-button", className].join(" ")}>
      {text}
    </Link>
  )
});

export default LinkButton;