import React, {useContext} from "react";
import PriceMeter from "../PriceMeter";
import {PriceMeterContext} from "../PriceMeter/context";
import "./price-meters.css";

const PriceMeters = () => {
  const {estimatedDaysBookedPerMonth = 1, estimatedEarningsPerBookedDay = {}, estimatedVehicleValue, recommendedDailyPrice} = useContext(PriceMeterContext);
  const {amount: pricePerDay = 0} =  estimatedEarningsPerBookedDay;
  const pricePerMonth = Math.floor(pricePerDay * estimatedDaysBookedPerMonth).toFixed(2);
  return (
    <div className="price-meters">
      <div className="inline-display">
        <PriceMeter mini text={estimatedDaysBookedPerMonth} description="Days booked per month" />
        <PriceMeter text={`$${pricePerMonth}`} description="Earnings per month (USD)" />
        <PriceMeter mini text={`$${pricePerDay}`} description="Earnings per day (USD)" />
      </div>
      <div className="price-est">{`Estimated vehicle value: $${estimatedVehicleValue.amount}**`}</div>
      <div className="price-est">{`Daily price: $${recommendedDailyPrice.amount}**`}</div>
    </div>
  )
};

export default PriceMeters