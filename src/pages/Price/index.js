import React from "react";
import LinkButton from "../../components/LinkButton";
import {PriceMeterProvider} from "../../components/PriceMeter/context";
import PriceMeters from "../../components/PriceMeters";
import RelatedCars from "../../components/RelatedCars";
import "./price.css";

const Price = (props) => {
  const {make, model, year, city} = props;
  return (
    <div className="price-page centered-container">
      <div className="description">
        <div className="car-info">
          <div className="primary">{`${make} ${model} ${year}`}</div>
          <div className="secondary">{city}</div>
        </div>
        <LinkButton className="change" to="/" text="Change" />
      </div>
      <PriceMeterProvider make={make} model={model} year={year} city={city}>
        <PriceMeters />
      </PriceMeterProvider>
      <RelatedCars />
    </div>
  );
}

export default Price;