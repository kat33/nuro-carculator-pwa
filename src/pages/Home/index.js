import React from "react";
import ModelSelector from "../../components/ModelSelector";
import RelatedCars from "../../components/RelatedCars";

import "./home.css";

const Home = () => {
  return (
    <div>
      <div className="banner">
        <div className="heading">Carculator</div>
        <div className="sub-heading">Discover what your car could earn for you</div>
        <div className="spacing">
            <ModelSelector />
        </div>    
      </div>
      <div className="centered-container padded-content">
        <RelatedCars />
      </div>
    </div>
  );
}

export default Home;